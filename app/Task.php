<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;


/**
 * Class Task
 * @package App
 *
 * @property int $id
 * @property string $body
 * @property string $created_at
 * @property string $updated_at
 * @property Project $project
 */
class Task extends Model
{
    protected $guarded = [];

    protected $touches = ['project'];

    /**
     * @return BelongsTo|Project
     */
    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function path(): string
    {
        return $this->project->path() . "/tasks/{$this->id}";
    }
}
