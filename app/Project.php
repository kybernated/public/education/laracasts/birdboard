<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;


/**
 * Class Project
 * @package App
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string created_at
 * @property string updated_at
 * @property int owner_id
 * @property User owner
 * @property Collection tasks
 */
class Project extends Model
{
    protected $guarded = [];

    public function path(): string
    {
        return "/projects/{$this->id}";
    }

    public function owner(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function tasks(): HasMany
    {
        return $this->hasMany(Task::class);
    }

    public function addTask($body): Model
    {
        return $this->tasks()->create(compact('body'));
    }
}
